package com.scb.email;

public interface EmailConstants {

    String TEMPLATE_HEADER = "templateHeader";
    String DEFAULT_HTML_PROPERTIES_FILE_PATH = "html-email.properties";
    String DEFAULT_HTML_SCHEMA_FILE_PATH = "/eproperties.json";
    String MAIN_HEADER = "mainHeader";
    String PIE_CHART_PATH = "/test-output/piechart.png";
    String PIE_CHART = "piechart";
    String SUB_HEADER = "";
    String EMAIL_HTML_TEMPLATE = "/EmailReportTemplate.html";
    String TEST_CASE_TABLE = "dataTable";
    String OUTPUT_PATH = "/test-output/EmailReport.html";
    String FOOTER = "footer";
}
