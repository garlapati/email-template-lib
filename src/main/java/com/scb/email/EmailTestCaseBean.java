package com.scb.email;

import com.google.gson.JsonObject;
import io.cucumber.plugin.event.Status;

public class EmailTestCaseBean {

    private JsonObject testcase;
    private String name;
    private Status status;

    public EmailTestCaseBean(){
        testcase = new JsonObject();
    }

    public void addProperty(String key, String value){
        testcase.addProperty(key, value);
    }

    public void setStatus(Status status){
        this.status = status;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public Status getStatus(){
        return status;
    }

    public JsonObject getTestcase(){
        return testcase;
    }

}
