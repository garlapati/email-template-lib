package com.scb.email;

public interface EmailProperties {

    String templateHeader = "html.templateHeader";
    String mainHeader = "html.mainHeader";
    String subHeader = "html.subHeader";
    String emailSubject = "html.emailSubject";
    String outputPath = "html.outputPath";
    String footer = "html.footer";
}
