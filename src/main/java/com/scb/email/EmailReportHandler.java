package com.scb.email;

import com.scb.common.Utils;
import io.cucumber.plugin.event.Status;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.RingPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.VerticalAlignment;
import org.jsoup.Jsoup;
import org.jsoup.internal.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.awt.*;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

public class EmailReportHandler {

    private static final Logger logger = LogManager.getLogger(EmailConcurrentEventListener.class.getName());
    static EmailReportHandler obj;
    ArrayList<EmailTestCaseBean> testCases = new ArrayList<>();
    Properties emailProperties;
    EmailTestCaseBean bean;
    Document htmlReport;
    String templateHeader;
    String mainHeader;
    String subHeader;
    Status overAllStatus = Status.PASSED;
    String outputPath;
    String emailSubject;


    public static EmailReportHandler getEmailReporter() {
        if (Objects.isNull(obj)) {
            obj = new EmailReportHandler();
        }
        return obj;
    }

    protected void addTest(String testCaseName) {
        bean = new EmailTestCaseBean();
        bean.setName(testCaseName);
    }

    protected void endTest(Status status) {
        bean.setStatus(status);
        testCases.add(bean);
    }

    public void addProperty(String key, String value) {
        bean.addProperty(key, value);
    }

    public void setEmailTemplateProperty(String key, String value) {
        emailProperties.setProperty(key, value);
    }

    public void setTestCases(ArrayList<EmailTestCaseBean> testCases) {
        this.testCases = testCases;
    }

    protected void initialize() throws Exception {
        logger.info("Initialize Properties");
        emailProperties = Utils.initialize().loadProperties(EmailConstants.DEFAULT_HTML_PROPERTIES_FILE_PATH, EmailConstants.DEFAULT_HTML_SCHEMA_FILE_PATH);
    }

    public void generateEmailReport() throws Exception {
        outputPath = emailProperties.getProperty(EmailProperties.outputPath) != null ? emailProperties.getProperty(EmailProperties.outputPath) : EmailConstants.OUTPUT_PATH;
        final File file = new File(System.getProperty("user.dir") + outputPath);
        if (!file.getParentFile().exists()) {
            boolean createdValue = file.getParentFile().mkdir();
            logger.info("directory created is " + createdValue);
        }
        this.htmlReport = loadhtmlfile();
        addHeadersToDocument();
        addPieChartToDocument();
        addTestCaseTable();
        updateFooter();
        FileUtils.writeStringToFile(file, htmlReport.outerHtml(), "UTF-8");
        logger.info(file + "file is created");
    }

    private Document loadhtmlfile() throws IOException {
        InputStream is = EmailReportHandler.class.getResourceAsStream(EmailConstants.EMAIL_HTML_TEMPLATE);
        return Jsoup.parse(is, "UTF-8", "");
    }

    private void setTextToElement(String value, String elementId) {
        Element element = htmlReport.getElementById(elementId);
        element.text(value);
    }

    private void addHeadersToDocument() {
        templateHeader = emailProperties.getProperty(EmailProperties.templateHeader) != null ? emailProperties.getProperty(emailProperties.getProperty(EmailProperties.templateHeader)) : "";
        mainHeader = emailProperties.getProperty(EmailProperties.mainHeader) != null ? emailProperties.getProperty(EmailProperties.mainHeader) : "";
        subHeader = emailProperties.getProperty(EmailProperties.subHeader) != null ? emailProperties.getProperty(EmailProperties.mainHeader) : "Details";
        setTextToElement(templateHeader, EmailConstants.TEMPLATE_HEADER);
        setTextToElement(mainHeader, EmailConstants.MAIN_HEADER);
        setTextToElement(subHeader, EmailConstants.SUB_HEADER);
    }

    private void updateFooter() {
        if (emailProperties.getProperty(EmailProperties.footer) != null) {
            setTextToElement(emailProperties.getProperty(EmailProperties.footer), EmailConstants.FOOTER);
        } else {
            htmlReport.getElementById("footerp").remove();
        }
    }

    private void addPieChartToDocument() throws IOException {
        String base64String = generatebase64StringforPieChart();
        Element element = htmlReport.getElementById(EmailConstants.PIE_CHART);
        element.attr("src", "data:image/jpeg;base64," + base64String);
    }

    private String generatebase64StringforPieChart() throws IOException {
        DefaultPieDataset data = constructPieChartData();
        RingPlot plot = setRingPlot(data);
        JFreeChart chart = setRingChart(plot);
        ChartRenderingInfo chartRendering = new ChartRenderingInfo(new StandardEntityCollection());
        File saveChart = new File(System.getProperty("user.dir") + EmailConstants.PIE_CHART_PATH);
        ChartUtilities.saveChartAsPNG(saveChart, chart, 1000, 300, chartRendering);
        File savedImage = new File(System.getProperty("user.dir") + EmailConstants.PIE_CHART_PATH);
        byte[] fileContent = FileUtils.readFileToByteArray(new File(savedImage.getAbsolutePath()));
        logger.info("Pie Chart is generated");
        return Base64.getEncoder().encodeToString(fileContent);
    }

    private DefaultPieDataset constructPieChartData() {
        int passed = 0;
        int failed = 0;
        int ignored = 0;

        for (EmailTestCaseBean testCase : testCases) {
            switch (testCase.getStatus()) {
                case PASSED:
                    passed++;
                    break;
                case FAILED:
                    failed++;
                    overAllStatus = testCase.getStatus();
                    break;
                default:
                    ignored++;
            }
        }
        DefaultPieDataset data = new DefaultPieDataset();
        data.setValue("Passed", passed);
        data.setValue("Failed", failed);
        data.setValue("Ignored", ignored);
        return data;
    }

    private RingPlot setRingPlot(DefaultPieDataset data) {
        RingPlot plot = new RingPlot(data);
        plot.setIgnoreZeroValues(true);
        plot.setSectionPaint("Passed", new Color(146, 208, 80));
        plot.setSectionPaint("Failed", new Color(255, 0, 0));
        plot.setSectionPaint("Ignored", new Color(192, 192, 192));

        plot.setSeparatorsVisible(false);
        plot.setSectionOutlinesVisible(false);
        plot.setSectionDepth(0.2);
        plot.setLabelBackgroundPaint(null);
        plot.setShadowPaint(null);
        plot.setLabelPaint(Color.BLACK);
        plot.setLabelShadowPaint(null);
        plot.setLabelOutlinePaint(null);
        plot.setLabelOutlineStroke(null);
        plot.setOutlinePaint(null);
        plot.setSimpleLabels(true);
        plot.setLabelFont(new Font("Arial", Font.BOLD, 16));
        plot.setNoDataMessage("No data available");
        plot.setLabelGap(0.02);
        plot.setLabelGenerator(new StandardPieSectionLabelGenerator(
                "{2}", new DecimalFormat("0"), new DecimalFormat("0%")));
        plot.setLegendLabelGenerator(new StandardPieSectionLabelGenerator(
                "{0} = {1}", new DecimalFormat("0"), new DecimalFormat("0%")));
        return plot;
    }

    private JFreeChart setRingChart(RingPlot plot) {
        JFreeChart chart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, true);
        LegendTitle title = chart.getLegend();
        title.setPosition(RectangleEdge.LEFT);
        title.setItemFont(new Font("Arial", Font.PLAIN, 16));
        title.setVerticalAlignment(VerticalAlignment.TOP);
        title.setBorder(0, 0, 0, 0);
        title.setMargin(new RectangleInsets(50.0, 0, 1.0, 150.0));
        chart.setBackgroundPaint(Color.white);
        chart.setBorderVisible(false);
        chart.getLegend().setFrame(BlockBorder.NONE);
        chart.setPadding(new RectangleInsets(0, 0, 0, 0));
        return chart;
    }

    private void addTestCaseTable() {
        Element tHead = constructTableHeader();
        Element tableBody = constructTableBody();
        Element element = htmlReport.getElementById(EmailConstants.TEST_CASE_TABLE);
        element.children().remove();
        tHead.appendTo(element);
        tableBody.appendTo(element);
    }


    private Element constructTableHeader() {
        Element thead = new Element("thead");
        Element tr = new Element("tr").addClass("header");
        Element th = new Element("th").attr("align", "center");
        th.text("No.").appendTo(tr);
        th = new Element("th");
        th.text("Test Case Name").appendTo(tr);
        testCases.get(0).getTestcase().keySet().forEach(key -> {
            Element Objth = new Element("th");
            Objth.text(key).appendTo(tr);
        });
        th = new Element("th");
        th.text("Status").appendTo(tr);
        tr.appendTo(thead);
        return thead;
    }

    private Element constructTableBody() {
        Element tBody = new Element("tbody");
        for (EmailTestCaseBean testCase : testCases) {
            Element tr = new Element("tr");
            Element td = new Element("td").attr("align", "center");
            int serialNo = testCases.indexOf(testCase) + 1;
            td.text(String.valueOf(serialNo)).appendTo(tr);
            td = new Element("td");
            td.text(testCase.getName()).appendTo(tr);
            testCase.getTestcase().keySet().forEach(key -> {
                Element objTd = new Element("td");
                Document parsedDoc = Jsoup.parseBodyFragment(testCase.getTestcase().get(key).getAsString());
                Element element = parsedDoc.body();
                element.appendTo(objTd);
                objTd.appendTo(tr);
            });
            td = new Element("td");
            String testStatus = testCase.getStatus().toString().toLowerCase();
            String status = (testStatus.equals("passed") || testStatus.equals("failed")) ? testStatus : "ignored";
            td.text(StringUtils.capitalize(status)).appendTo(tr);
            tr.appendTo(tBody);
        }
        return tBody;
    }


    public void generateOverAllStatustxt() throws FileNotFoundException {
        emailSubject = emailProperties.getProperty(EmailProperties.emailSubject) != null ? emailProperties.getProperty(EmailProperties.emailSubject) : "";
        File txtFile = new File(System.getProperty("user.dir") + outputPath);
        String filename = txtFile.getParent() + "/Status.txt";
        OutputStream txtOut = new BufferedOutputStream(new FileOutputStream(filename));
        PrintWriter txtWriter = new PrintWriter(txtOut);
        txtWriter.println("[" + overAllStatus + "]".concat(emailSubject));
        txtWriter.close();
        logger.info(filename + " file is created");

    }
}
