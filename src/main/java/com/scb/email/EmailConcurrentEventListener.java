package com.scb.email;

import io.cucumber.plugin.ConcurrentEventListener;
import io.cucumber.plugin.event.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmailConcurrentEventListener implements ConcurrentEventListener {

    EmailReportHandler handler;
    private static final Logger logger = LogManager.getLogger(EmailConcurrentEventListener.class.getName());

    @Override
    public void setEventPublisher(EventPublisher publisher) {

        publisher.registerHandlerFor(TestCaseStarted.class, testCaseStartedHandler);
        publisher.registerHandlerFor(TestCaseFinished.class, testCaseFinishedEventHandler);
        publisher.registerHandlerFor(TestRunFinished.class, testRunFinishedHandler);
    }

    public EmailConcurrentEventListener(String args){
        try{
            logger.info("Started Generation of html email properties");
            handler = EmailReportHandler.getEmailReporter();
            handler.initialize();
        }catch (Exception e){
            logger.error(e);
            System.exit(0);
        }
    }

    private EventHandler<TestCaseStarted> testCaseStartedHandler = new EventHandler<TestCaseStarted>() {
        @Override
        public void receive(TestCaseStarted event) {
            handler.addTest(event.getTestCase().getName());
        }
    };

    private EventHandler<TestCaseFinished> testCaseFinishedEventHandler = new EventHandler<TestCaseFinished>() {
        @Override
        public void receive(TestCaseFinished event) {
            handler.endTest(event.getResult().getStatus());
        }
    };

    private EventHandler<TestRunFinished> testRunFinishedHandler = new EventHandler<TestRunFinished>() {
        @Override
        public void receive(TestRunFinished event) {
            try{
                handler.generateEmailReport();
                handler.generateOverAllStatustxt();

            } catch (Exception e) {
                logger.error(e);
                System.exit(0);
            }
        }
    };


}
