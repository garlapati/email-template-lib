package com.scb.common;


import com.google.gson.Gson;
import com.scb.email.EmailProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

public class Utils {

    private static final Logger logger = LogManager.getLogger(Utils.class.getName());
    static Utils utils;

    public static Utils initialize(){
        if(Objects.isNull(utils)){
            utils = new Utils();
        }
        return utils;
    }

    public Properties loadProperties(String propertyFile, String propertySchema) throws Exception{
        ClassLoader loader = EmailProperties.class.getClassLoader();
        InputStream inputStream = loader.getResourceAsStream(propertyFile);
        if(Objects.isNull(inputStream)){
            logger.warn(propertyFile + " - file is not found in resource folder default values will be used");
            inputStream = new ByteArrayInputStream("".getBytes());
        }
        Properties properties = new Properties();
        properties.load(inputStream);
        Gson gson = new Gson();
        String propJson = gson.toJson(properties);
        validateResponseWithSchema(propJson, propertySchema, propertyFile);
        return properties;
    }

    private static void validateResponseWithSchema(String responseText, String propertySchema, String propertyFile)  throws Exception{

        StringBuilder messages = new StringBuilder();
        try{
            JSONObject jsonSchema = new JSONObject(new JSONTokener(Utils.class.getResourceAsStream(propertySchema)));
            JSONObject jsonSubject = new JSONObject(new JSONTokener(responseText));
            Schema schema = SchemaLoader.load(jsonSchema);
            schema.validate(jsonSubject);
        } catch (ValidationException exception) {
            List<ValidationException> exceptionList = exception.getCausingExceptions();
            for (ValidationException e : exceptionList) {
                String message = e.getMessage() + " : default value/setEmailProperties vlaues will be used";
                messages.append(System.getProperty("line.separator"));
                messages.append(message);
            }
            if (exceptionList.size()==0){
                String message = exception.getMessage() + " in " + propertyFile + " file";
                messages.append(message);
                messages.append(System.getProperty("line.seperator"));
            }
            logger.warn(messages.toString());
        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
}
